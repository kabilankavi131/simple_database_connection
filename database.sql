-- database creation
CREATE DATABASE db_contact;

-- table creation 
USE db_contact;
CREATE TABLE tbl_contact (
    txtName VARCHAR(255),
    txtEmail VARCHAR(255),
    txtPhone VARCHAR(20),
    txtMessage TEXT
);

